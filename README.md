# python MDORbuilder

## Introduction
This tool is made to build up a MDOR without manual intervention on the data, avoiding cut and paste of binary ASCII data or other files handling.
The tool offers some check angain the data and some memory mapping to help the user.
You can choose the Memory Type and page, specify if you want to write on RAM or EEPROM (this is simply a check) and, of course the address.

A error message is rised if the data exeed the page size, if the user specified a wrong memory use (RAM or EEPROM) respect to the page or if the data will overwrite a forbidden area.

## usage

```
python MDORbuilder.py ICEThcFixImage.bin MDOR_template_1.BC 0007 DM RAM 01 0000
```

`ICEThcFixImage.bin` is the bin file to be patched

`MDOR_template_1.BC` is the template to use for the MDOR

`0007` is the output id of the MDOR

`DM` DM|PM is the memory type

`RAM` RAM|EEPROM is the memory area

`01` is the page

`0000` is the starting address

### Memory map library 

__memorydecode function__


    Memory deconding function for address
	usage: `memorydecode( startaddress, endaddress)` 
	split the address 0x0A23DE in: page(0x0A) + local address 4digit(0x23DE)

	output: slice for address and slice for pages
	
__memoryPagePlot function__
	
	Dump a memory page plot into a png file to visualize the correct structure of your memory
	usage: `memoryPagePlot(page, memoryType)`
	`page` = page number
	`memoryType` = 'DM|PM'
	
	output: A png file in the root of your script
	
	
	
	
## Dependencies

* etree
* numpy
* matplotlib

## Version

This is the starting version 0.1
Not support on errors and exit. Please, check the outup file.

## Disclaimer

This tool is thinked to be used only on ISA memory. A different memory mapping file is required for other instrument. **The author does not assume any responsibily on the damage to the instruments that decide to use this tool**