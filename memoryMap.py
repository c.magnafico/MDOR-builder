#!/usr/bin/env python
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import math


#set some errors
class wrongrequest(Exception):
    pass



page_size = 256*256

#memory address: local first 4digit(0x0000). page(0x00)
ISA_DataMemory = np.empty((page_size,12), dtype=object )
ISA_ProgamMemory = np.empty((page_size,5), dtype=object )

#Define a way to quick take memory adress
def memorydecode( start_address, end_address):
	"""
	Memory deconding function for adressing decoding
	usage: memorydecode( start_address, end_address)
	page(0x00) + local address 4digit(0x0000)

	output: slice for address and slice for pages.
	"""
	p = slice( int(start_address[0:2],16), int(end_address[0:2],16)+1) #CONTROLLARE QUA se occorre un +1!!!
	i = slice( int(start_address[2:6],16), int(end_address[2:6],16)+1)
	
	if  int(start_address[2:6],16) == int(end_address[2:6],16):
	 	i = int(start_address[2:6],16)
	if  int(start_address[0:2],16) == int(end_address[0:2],16):
	 	p = int(start_address[0:2],16)

	return i,p

#memory mapping DATA MEMORY

# Map
# {'type':'EEPROM','status':'free','value':0,'sect':'EEPROM-0'}

#PROM
#prom-0 set
ISA_DataMemory[ memorydecode('0b0000', '0b8000')  ] = {'type':'PROM','sect':'PROM-0','status':'used'}

#RAM
#ram-3 set
ISA_DataMemory[ memorydecode('008000', '0bA69F')  ] = {'type':'RAM','sect':'RAM-3','status':'used'}
ISA_DataMemory[ memorydecode('00A69F', '0bEFFF')  ] = {'type':'RAM','sect':'RAM-3','status':'free'}

#ram-4 set
ISA_DataMemory[ memorydecode('000000', '008000')  ] = {'type':'RAM','sect':'RAM-4','status':'free'}

#ram-5 set
ISA_DataMemory[ memorydecode('010000', '018000')  ] = {'type':'RAM','sect':'RAM-5','status':'used'}

#ram-6 set
ISA_DataMemory[ memorydecode('020000', '028000')  ] = {'type':'RAM','sect':'RAM-6','status':'used'}

#ram-7 set
ISA_DataMemory[ memorydecode('030000', '038000')  ] = {'type':'RAM','sect':'RAM-7','status':'free'}

#ram-0 set
ISA_DataMemory[ memorydecode('080000', '088000')  ] = {'type':'RAM','sect':'RAM-0','status':'used'}

#ram-1 set
ISA_DataMemory[ memorydecode('090000', '098000')  ] = {'type':'RAM','sect':'RAM-1','status':'used'}

#ram-2 set
ISA_DataMemory[ memorydecode('0a0000', '0a8000')  ] = {'type':'RAM','sect':'RAM-2','status':'used'}


#EEPROM
#eeprom-0 set
ISA_DataMemory[ memorydecode('040000', '048000')  ] = {'type':'EEPROM','sect':'EEPROM-0','status':'used'}

#eeprom-1 set
ISA_DataMemory[ memorydecode('050000', '058000')  ] = {'type':'EEPROM','sect':'EEPROM-1','status':'used'}

#eeprom-2 set
ISA_DataMemory[ memorydecode('060000', '068000')  ] = {'type':'EEPROM','sect':'EEPROM-2','status':'used'}

#eeprom-3 set
ISA_DataMemory[ memorydecode('070000', '070600')  ] = {'type':'EEPROM','sect':'EEPROM-2','status':'used'}
ISA_DataMemory[ memorydecode('070600', '078000')  ] = {'type':'EEPROM','sect':'EEPROM-2','status':'free'}



#AMBA APB
ISA_DataMemory[ memorydecode('00F000', '0bFFFF')  ] = {'type':'PROM','sect':'PROM-0','status':'reserved'}




#memory mapping PROGRAM MEMORY

#PROM
#prom set
ISA_ProgamMemory[ memorydecode('000000', '008000')  ] = {'type':'PROM','sect':'PROM','status':'used'}
ISA_ProgamMemory[ memorydecode('00798F', '007FFD')  ] = {'type':'PROM','sect':'PROM','status':'free'}


#EEPROM
#eeprom-0 set
ISA_ProgamMemory[ memorydecode('008000', '00FFFF')  ] = {'type':'EEPROM','sect':'EEPROM-0','status':'used'}
ISA_ProgamMemory[ memorydecode('048000', '04FFFF')  ] = {'type':'EEPROM','sect':'EEPROM-0','status':'used'}

#eeprom-1 set
ISA_ProgamMemory[ memorydecode('010000', '018000')  ] = {'type':'EEPROM','sect':'EEPROM-1','status':'used'}

#eeprom-2 set
ISA_ProgamMemory[ memorydecode('010000', '01FFFD')  ] = {'type':'EEPROM','sect':'EEPROM-2','status':'used'}
ISA_ProgamMemory[ memorydecode('01FFFD', '01FFFF')  ] = {'type':'EEPROM','sect':'EEPROM-2','status':'reserved'}
ISA_ProgamMemory[ memorydecode('01DAA4', '01FFFD')   ]= {'type':'EEPROM','sect':'EEPROM-2','status':'free'}


#RAM
#ram-0 set
ISA_ProgamMemory[ memorydecode('020000', '028000')  ] = {'type':'RAM','sect':'RAM-0','status':'free'}
ISA_ProgamMemory[ memorydecode('030000', '038000')  ] = {'type':'RAM','sect':'RAM-0','status':'free'}
ISA_ProgamMemory[ memorydecode('027FFE', '028000')  ] = {'type':'RAM','sect':'RAM-0','status':'reserved'}
ISA_ProgamMemory[ memorydecode('037FFE', '038000')  ] = {'type':'RAM','sect':'RAM-0','status':'reserved'}
#ram-1 set
ISA_ProgamMemory[ memorydecode('028000', '02FFFF')  ] = {'type':'RAM','sect':'RAM-1','status':'used'}
ISA_ProgamMemory[ memorydecode('02DAA4', '02FFFD')  ] = {'type':'RAM','sect':'RAM-1','status':'free'}
ISA_ProgamMemory[ memorydecode('02FFFD', '02FFFF')  ] = {'type':'RAM','sect':'RAM-1','status':'reserved'}

#ram-2 set
ISA_ProgamMemory[ memorydecode('038000', '03FFFF')  ] = {'type':'RAM','sect':'RAM-2','status':'free'}
ISA_ProgamMemory[ memorydecode('040000', '048000')  ] = {'type':'RAM','sect':'RAM-2','status':'used'}



def memoryPagePlot(page, memoryType):

	#to display memory in such a way
	cluster = int(256/2)
	pagesize = int(256*256)
	page=format(page,'02X')

	
	if memoryType is 'DataMemory':
		memorypage = ISA_DataMemory[memorydecode(page+'0000',page+'FFFF')]
	elif memoryType is 'ProgramMemory':
		memorypage = ISA_ProgamMemory[memorydecode(page+'0000',page+'FFFF')]
	else:
		raise wrongrequest('you can specify only memoryType = DataMemory | ProgramMemory')
	

	A = np.empty((pagesize/cluster,cluster,3), dtype=float )
	R = np.random.random_sample((pagesize,))
	indx=0
	for MM in memorypage:
		
		i = int(math.ceil(indx/cluster));
		j = int(indx%cluster)

		if MM['status'] is 'free':
			A[i,j,:] = [102./255,1-R[indx]*0.2,71./255]
		elif MM['status'] is 'used':
			A[i,j,:] = [71./255,132./255,1-R[indx]*0.2]
		elif MM['status'] is 'reserved':
			A[i,j,:] = [1-R[indx]*0.2,38./255,0]


		indx += 1



	label_ticky = np.linspace(0, pagesize,num=9,dtype=int)
	label_ticky_h =[ '%s'%format(label_ticky[i],'#06X') for i in range(0,len(label_ticky))]
	label_ticky_h[-1] = '%s'%format(label_ticky[-1]-1,'#06X')

	label_tickx = np.linspace(0, cluster,num=5,dtype=int)
	label_tickx_h =[ '%s'%format(label_tickx[i],'#04X') for i in range(0,len(label_tickx))]

	my_dpi = 90

	plt.figure(1,figsize=(1200/my_dpi, 1200/my_dpi), dpi=my_dpi, facecolor='w', edgecolor='k')
	plt.imshow(A)
	plt.gca().invert_yaxis()
	plt.yticks(label_ticky/cluster,label_ticky_h)
	plt.xticks(label_tickx,label_tickx_h, rotation=70)
	plt.clim()
	plt.grid()
	reserved_patch = mpatches.Patch(color='red', label='Reserved Memory')
	free_patch = mpatches.Patch(color='green', label='Free Memory')
	used_patch = mpatches.Patch(color='blue', label='Used Memory')
	plt.legend(handles=[used_patch,free_patch,reserved_patch],bbox_to_anchor=(0, 1), loc='lower left', ncol=3)
	plt.savefig('view'+memoryType+'_page'+page+'.png', dpi=my_dpi)
	print('saved memory image: '+'view'+memoryType+'_page'+page+'.png')